# pwman3 po-debconf translation to Spanish.
# Copyright (C) 2021
# This file is distributed under the same license as the pwman3 package.
# Camaleón <noelamac@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: pwman3\n"
"Report-Msgid-Bugs-To: pwman3@packages.debian.org\n"
"POT-Creation-Date: 2020-01-07 08:39+0000\n"
"PO-Revision-Date: 2021-04-16 17:56+0200\n"
"Last-Translator: Camaleón <noelamac@gmail.com>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../templates:1001
msgid "Old database format detected"
msgstr "Se ha detectado un formato obsoleto de la base de datos"

#. Type: select
#. Description
#: ../templates:1001
msgid ""
"It seems that you are trying to upgrade Pwman3 from version 0.5.x or older. "
"The database is not compatible with the new database format. Before "
"upgrading you need to export your database to a CSV with:"
msgstr ""
"Parece que está intentando actualizar Pwman3 desde la versión 0.5.x o "
"anterior. La base de datos no es compatible con el nuevo formato de base "
"de datos. Antes de actualizar, debe exportar su base de datos a un formato CSV, "
"con la orden:"

#. Type: select
#. Description
#: ../templates:1001
msgid "  pwman> export"
msgstr "  pwman> export"

#. Type: select
#. Description
#: ../templates:1001
msgid ""
"That will create a CSV file located in $HOME/pwman-export.csv. Once "
"exported, you will have to rename your old database to keep a backup of it:"
msgstr ""
"Se creará un archivo CSV en «$HOME/pwman-export.csv». Una vez exportada, "
"tendrá que renombrar la base de datos antigua para mantener una copia de "
"seguridad, con la orden:"

#. Type: select
#. Description
#: ../templates:1001
msgid "  mv $HOME/pwman/pwman.db $HOME/pwman/pwman-old.db"
msgstr "  mv $HOME/pwman/pwman.db $HOME/pwman/pwman-old.db"

#. Type: select
#. Description
#: ../templates:1001
msgid ""
"Then you can restart the package upgrade. Once the upgrade will be finished, "
"you will be able to import the CSV file previously generated:"
msgstr ""
"Entonces podrá reiniciar la actualización del paquete. Cuando termine la "
"actualización, podrá importar el archivo CSV creado anteriormente, con la "
"orden:"

#. Type: select
#. Description
#: ../templates:1001
msgid "  pwman3 -i $HOME/pwman-export.csv \\;"
msgstr "  pwman3 -i $HOME/pwman-export.csv \\;"

#. Type: select
#. Description
#: ../templates:1001
msgid ""
"Don't forget to remove the CSV file when the import succeeded (the passwords "
"are stored in clear text in this file)."
msgstr ""
"No se olvide de eliminar el archivo CSV cuando la importación termine "
"correctamente (en este archivo, las contraseñas se almacenan en texto claro)."
